//
//  AnswerCollectionViewCell.swift
//  Trivia
//
//  Created by MyMacbook on 11/23/17.
//  Copyright © 2017 MyMacbook. All rights reserved.
//

import UIKit

class AnswerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var chooseContent: UIView!
    @IBOutlet weak var choiceLabel: UILabel!
    @IBOutlet weak var answerQuestionContent: UIView!
    @IBOutlet weak var answerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        chooseContent.layer.borderWidth = 1
        chooseContent.layer.borderColor = UIColor.lightGray.cgColor
        chooseContent.layer.cornerRadius = chooseContent.bounds.size.width/2
        answerQuestionContent.layer.borderWidth = 1
        answerQuestionContent.layer.borderColor = UIColor.black.cgColor
        answerQuestionContent.layer.cornerRadius = 10
    }
    
}

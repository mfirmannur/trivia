//
//  CategoryCollectionViewCell.swift
//  Trivia
//
//  Created by MyMacbook on 11/22/17.
//  Copyright © 2017 MyMacbook. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryContainer: UIView!
    @IBOutlet weak var categoryLogo: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

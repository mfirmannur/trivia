//
//  TriviaCategoryPage.swift
//  Trivia
//
//  Created by MyMacbook on 11/22/17.
//  Copyright © 2017 MyMacbook. All rights reserved.
//

import UIKit

class TriviaCategoryPage: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    var categories: [CategoryModel] = []
    var listOfQuestion:QuestionListModel?
    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var loadingView: UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        let flow: UICollectionViewFlowLayout = categoryCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        self.navigationController?.navigationBar.barTintColor = UIColor().hexStringToUIColor(hex: "#9012fe")
        getCategoryData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if UserDefaults.standard.object(forKey: "\(categories[indexPath.row].id!)") != nil {
            let questionList = QuestionListModel(dictionary: UserDefaults.standard.object(forKey: "\(categories[indexPath.row].id!)") as! NSDictionary)
            self.listOfQuestion = questionList
            self.performSegue(withIdentifier: "goToQuestionPage", sender: categories[indexPath.row])
        } else {
            if !Reachability.isConnectedToNetwork(){
                onNoConnection()
            } else {
                getQuestionData(category: categories[indexPath.row])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width/2)-15, height: 138)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
        cell.categoryContainer.layer.cornerRadius = 5
        cell.categoryTitle.text = categories[indexPath.row].name!
        cell.categoryContainer.backgroundColor = UIColor(patternImage: UIImage(named: categories[indexPath.row].background!)!)
        cell.categoryLogo.image = UIImage(named: categories[indexPath.row].logo!)
        return cell
    }
    
    func getCategoryData() {
        categories = []
        let path = Bundle.main.path(forResource:"Category", ofType: "json")
        var jsonData: Data?
        do {
            jsonData = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
        } catch {
            jsonData = nil
        }
        do {
            let dictionaryResponse: NSDictionary = try JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            let categoriesData = dictionaryResponse.object(forKey: "trivia_categories") as! NSArray
            for category in categoriesData {
                categories.append(CategoryModel(dictionary: category as! NSDictionary))
            }
            categoryCollectionView.reloadData()
        } catch {
            
        }
    }
    
    func getQuestionData(category: CategoryModel) {
        showActivityIndicator()
        let url = URL(string: "https://opentdb.com/api.php?amount=20&category=\(category.id!)&type=multiple")
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                self.hideActivityIndicator()
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    
                    OperationQueue.main.addOperation({
                        self.listOfQuestion = QuestionListModel(dictionary: json as NSDictionary)
                        UserDefaults.standard.setValue(self.listOfQuestion?.toDictionary(), forKey: "\(category.id!)")
                        self.hideActivityIndicator()
                        self.performSegue(withIdentifier: "goToQuestionPage", sender: category)
                    })
                    
                }catch let error as NSError{
                    self.hideActivityIndicator()
                    print(error)
                }
            }
        }).resume()
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async() {
            self.loadingView = UIView()
            self.loadingView.frame = CGRect(x: 0.0, y: 0.0, width: 100.0, height: 100.0)
            self.loadingView.center = self.view.center
            self.loadingView.backgroundColor = UIColor().hexStringToUIColor(hex: "#444444")
            self.loadingView.alpha = 0.7
            self.loadingView.clipsToBounds = true
            self.loadingView.layer.cornerRadius = 10
            
            self.spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            self.spinner.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0)
            self.spinner.center = CGPoint(x:self.loadingView.bounds.size.width / 2, y:self.loadingView.bounds.size.height / 2)
            
            self.loadingView.addSubview(self.spinner)
            self.view.addSubview(self.loadingView)
            self.spinner.startAnimating()
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async() {
            self.spinner.stopAnimating()
            self.loadingView.removeFromSuperview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToQuestionPage" {
            let questionPage = segue.destination as! CategoryQestionPage
            questionPage.questionList = (self.listOfQuestion?.questionList)!
            questionPage.titlePage = (sender as! CategoryModel).name!
            questionPage.id = (sender as! CategoryModel).id!
        }
    }
    
    func onNoConnection() {
        let message = "No internet connection."
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

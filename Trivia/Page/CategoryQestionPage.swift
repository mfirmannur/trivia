//
//  CategoryQestionPage.swift
//  Trivia
//
//  Created by MyMacbook on 11/23/17.
//  Copyright © 2017 MyMacbook. All rights reserved.
//

import UIKit

class CategoryQestionPage: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerCollectionView: UICollectionView!
    @IBOutlet weak var questionContentView: UIView!
    @IBOutlet weak var questionInfoContentView: UIView!
    @IBOutlet weak var numberOfQuestion: UILabel!
    @IBOutlet weak var correctAnswerCountLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var choiceArray: [String] = ["A","B","C","D"]
    var questionList: [QuestionModel] = []
    var currentQuestion: QuestionModel?
    var titlePage = ""
    var questionNumber: Int = 1
    var numberOfCorrectAnswer: Int = 0
    var indexAnswer = 0
    var hasChooseAnswer = false
    var id: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let changeTitle = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = changeTitle
        setMultipleLineTitle(titlePage)
        answerCollectionView.dataSource = self
        answerCollectionView.delegate = self
        let flow: UICollectionViewFlowLayout = answerCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.sectionInset = UIEdgeInsetsMake(10, 5, 10, 5)
        currentQuestion = questionList[0]
        numberOfQuestion.text = "Question no. \(questionNumber)"
        correctAnswerCountLabel.text = "\(numberOfCorrectAnswer) Right Answers"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentQuestion!.answersList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width/2)-15, height: 112)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let answerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnswerCollectionViewCell", for: indexPath as IndexPath) as! AnswerCollectionViewCell
        let listOfAnswer = currentQuestion?.answersList[indexPath.row]
        answerCell.choiceLabel.text = choiceArray[indexPath.row]
        if !hasChooseAnswer {
            answerCell.answerLabel.text = listOfAnswer
            answerCell.answerLabel.textColor = UIColor.black
            answerCell.choiceLabel.textColor = UIColor.black
            answerCell.answerQuestionContent.backgroundColor = UIColor.white
        } else {
            if listOfAnswer == currentQuestion?.correctAnswer {
                answerCell.answerLabel.text = listOfAnswer! + "\nCorrect Answer"
                answerCell.answerLabel.textColor = UIColor.white
                answerCell.choiceLabel.textColor = UIColor().hexStringToUIColor(hex:  "#50e3c2")
                answerCell.answerQuestionContent.backgroundColor = UIColor().hexStringToUIColor(hex:  "#50e3c2")
            } else {
                if indexAnswer == indexPath.row && listOfAnswer != currentQuestion?.correctAnswer {
                    answerCell.answerLabel.textColor = UIColor.white
                    answerCell.answerLabel.text = "\(listOfAnswer!)\nWrong Answer!"
                    answerCell.choiceLabel.textColor = UIColor().hexStringToUIColor(hex:  "#fb4040")
                    answerCell.answerQuestionContent.backgroundColor = UIColor().hexStringToUIColor(hex:  "#fb4040")
                } else {
                    answerCell.answerLabel.text = listOfAnswer
                    answerCell.answerLabel.textColor = UIColor.black
                    answerCell.choiceLabel.textColor = UIColor.black
                    answerCell.answerQuestionContent.backgroundColor = UIColor.white
                }
            }
        }
        return answerCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !hasChooseAnswer {
            hasChooseAnswer = true
            indexAnswer = indexPath.row
            if currentQuestion?.answersList[indexAnswer] == currentQuestion?.correctAnswer {
                numberOfCorrectAnswer += 1
                setCountOfCorrectAnswer()
            }
            collectionView.reloadData()
        }
    }

    /**
     Set multiple title on the header navigation controller. Add UILabel and set multple of number of lines.
     - Parameter title: A string to set a title.
     */
    public func setMultipleLineTitle(_ title: String) {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        label.text = title
        label.numberOfLines = 2
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        navigationItem.titleView = label
    }

    @IBAction func onTapNextButton(_ sender: Any) {
        if hasChooseAnswer {
            hasChooseAnswer = false
            questionNumber += 1
            if questionNumber <= questionList.count {
                currentQuestion = questionList[questionNumber-1]
                setQuestion()
                setNumberOfQuestion()
                if questionNumber == questionList.count {
                    nextButton.setTitle("FINISH", for: .normal)
                }
                answerCollectionView.reloadData()
            } else {
                onFinishQuiz()
            }
        } else {
            onNotYetChooseAnswer()
        }
    }
    
    func setQuestion() {
        questionLabel.text = currentQuestion?.question
    }
    
    func setNumberOfQuestion() {
        numberOfQuestion.text = "Question no. \(questionNumber)"
    }
    
    func setCountOfCorrectAnswer() {
        correctAnswerCountLabel.text = numberOfCorrectAnswer == 1 ? "\(numberOfCorrectAnswer) Right Answer" : "\(numberOfCorrectAnswer) Right Answers"
    }
    
    func onFinishQuiz() {
        let message = "Finished, you've got \(numberOfCorrectAnswer) correct answers."
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.removeObject(forKey: "\(self.id)")
            self.navigationController?.popViewController(animated: true)
        })
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func onNotYetChooseAnswer() {
        let message = "Not yet choose the answer, Please Choose the answer below."
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

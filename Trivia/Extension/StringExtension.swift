//
//  StringExtension.swift
//  Trivia
//
//  Created by MyMacbook on 11/24/17.
//  Copyright © 2017 MyMacbook. All rights reserved.
//

import UIKit

extension String {
    
    func convertHtmlSymbols() throws -> String? {
        guard let data = data(using: .utf8) else { return nil }
        
        return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil).string
    }
}
